package com.nerds.stocks.data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import com.nerds.stocks.db.entities.StatsEntity;
import com.nerds.stocks.db.entities.StockEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode @ToString
@Getter @Setter
public class Stats {
    private BigDecimal sharesFloat;
    private BigDecimal sharesOutstanding;
    private BigDecimal sharesOwned;
    
    private BigDecimal eps;
    private BigDecimal pe;
    private BigDecimal peg;
    
    private BigDecimal epsEstimateCurrentYear;
    private BigDecimal epsEstimateNextQuarter;
    private BigDecimal epsEstimateNextYear;
    
    private BigDecimal priceBook;
    private BigDecimal priceSales;
    private BigDecimal bookValuePerShare;
    
    private BigDecimal revenue; // ttm
    private BigDecimal EBITDA; // ttm
    private BigDecimal dividend;
    private BigDecimal oneYearTargetPrice;
    
    private BigDecimal shortRatio;

    private Calendar earningsAnnouncement;

    public Stats(StatsEntity statsObj) {
        setSharesFloat(statsObj.getSharesFloat());
        setSharesOutstanding(statsObj.getSharesOutstanding());
        setSharesOwned(statsObj.getSharesOwned());
        setEps(statsObj.getEps());
        setPe(statsObj.getPe());
        setPeg(statsObj.getPeg());
        setEpsEstimateCurrentYear(statsObj.getEpsEstimateCurrentYear());
        setEpsEstimateNextQuarter(statsObj.getEpsEstimateNextQuarter());
        setEpsEstimateNextYear(statsObj.getEpsEstimateNextYear());

        setPriceBook(statsObj.getPriceBook());
        setPriceSales(statsObj.getPriceSales());
        setBookValuePerShare(statsObj.getBookValuePerShare());
        setRevenue(statsObj.getRevenue());
        setEBITDA(statsObj.getEBITDA());
        setDividend(statsObj.getDividend());
        setOneYearTargetPrice(statsObj.getOneYearTargetPrice());
        setShortRatio(statsObj.getShortRatio());
        Calendar updateTime = Calendar.getInstance();
        if(statsObj.getNextEarningsDate() != null) {
            updateTime.setTimeInMillis(statsObj.getNextEarningsDate().getTime());
        }
        
        setEarningsAnnouncement(updateTime);
        
    }
    
    public Stats() {

    }

    public StatsEntity toStatsEntity(StockEntity stock) {
    	StatsEntity statsEntity = new StatsEntity();
    	statsEntity.setSharesFloat(this.getSharesFloat());
    	statsEntity.setSharesOutstanding(this.getSharesOutstanding());
    	statsEntity.setSharesOwned(this.getSharesOwned());
    	statsEntity.setEps(this.getEps());
    	statsEntity.setPe(this.getPe());
    	statsEntity.setPeg(this.getPeg());
    	statsEntity.setEpsEstimateCurrentYear(this.getEpsEstimateCurrentYear());
    	statsEntity.setEpsEstimateNextQuarter(this.getEpsEstimateNextQuarter());
    	statsEntity.setEpsEstimateNextYear(this.getEpsEstimateNextYear());

    	statsEntity.setPriceBook(this.getPriceBook());
    	statsEntity.setPriceSales(this.getPriceSales());
    	statsEntity.setBookValuePerShare(this.getBookValuePerShare());
    	statsEntity.setRevenue(this.getRevenue());
    	statsEntity.setEBITDA(this.getEBITDA());
    	statsEntity.setOneYearTargetPrice(this.getOneYearTargetPrice());
    	statsEntity.setShortRatio(this.getShortRatio());
        
    	statsEntity.setDividend(this.getDividend());
        //setYieldPercent(this.getYieldPercent());
        if(this.getEarningsAnnouncement() != null) {
        	statsEntity.setNextEarningsDate(new Timestamp(this.getEarningsAnnouncement().getTimeInMillis()));
        }else {
        	statsEntity.setNextEarningsDate(new Timestamp(new Date().getTime()));
        }
        statsEntity.setStock(stock);
        return statsEntity;
    }
    
}