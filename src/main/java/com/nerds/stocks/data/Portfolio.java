package com.nerds.stocks.data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.nerds.stocks.db.entities.PortfolioEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @AllArgsConstructor
@Getter @Setter
public class Portfolio {
    String portfolioName;
    List<Transaction> holdings = new ArrayList<Transaction>();

    public Portfolio(PortfolioEntity entity) {
        this.portfolioName = entity.getPortfolioName();
        holdings = entity.getHoldings().stream().map(tranEntity -> new Transaction(tranEntity)).collect(Collectors.toList());
    }

}