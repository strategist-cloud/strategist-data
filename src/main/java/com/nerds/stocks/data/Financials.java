package com.nerds.stocks.data;

import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nerds.stocks.db.entities.FinancialsEntity;
import com.nerds.stocks.db.entities.StockEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@Getter
@Setter
public class Financials {
    @JsonIgnore
    Fundamentals stock;

    LocalDate reportDate;
    Double grossProfit;
    Double costOfRevenue;
    Double operatingRevenue;
    Double totalRevenue;
    Double operatingIncome;
    Double netIncome;
    Double researchAndDevelopment;
    Double operatingExpense;
    Double currentAssets;
    Double totalAssets;
    Double totalLiabilities;
    Double currentCash;
    Double currentDebt;
    Double totalCash;
    Double totalDebt;
    Double shareholderEquity;
    Double cashChange;
    Double cashFlow;
    Double operatingGainsLosses;

    public Financials() {

    }

    public Financials(FinancialsEntity statsObj, Fundamentals stock) {
        if (statsObj.getReportDate() != null) {
            setReportDate(statsObj.getReportDate().toLocalDate());
        }

        setGrossProfit(statsObj.getGrossProfit());
        setCostOfRevenue(statsObj.getCostOfRevenue());
        setOperatingRevenue(statsObj.getOperatingRevenue());
        setTotalRevenue(statsObj.getTotalRevenue());
        setOperatingIncome(statsObj.getOperatingIncome());
        setNetIncome(statsObj.getNetIncome());
        setResearchAndDevelopment(statsObj.getResearchAndDevelopment());
        setOperatingExpense(statsObj.getOperatingExpense());
        setCurrentAssets(statsObj.getCurrentAssets());
        setTotalAssets(statsObj.getTotalAssets());
        setTotalLiabilities(statsObj.getTotalLiabilities());
        setCurrentCash(statsObj.getCurrentCash());
        setCurrentDebt(statsObj.getCurrentDebt());
        setTotalCash(statsObj.getTotalCash());
        setTotalDebt(statsObj.getTotalDebt());
        setShareholderEquity(statsObj.getShareholderEquity());
        setCashChange(statsObj.getCashChange());
        setCashFlow(statsObj.getCashFlow());
        setOperatingGainsLosses(statsObj.getOperatingGainsLosses());

        setStock(stock);
    }

    public FinancialsEntity toFinancialsEntity(StockEntity stockEntity) {
        FinancialsEntity financials = new FinancialsEntity();
        if (this.getReportDate() != null) {
            financials.setReportDate(Date.valueOf(this.getReportDate()));
        }

        financials.setGrossProfit(this.getGrossProfit());
        financials.setCostOfRevenue(this.getCostOfRevenue());
        financials.setOperatingRevenue(this.getOperatingRevenue());
        financials.setTotalRevenue(this.getTotalRevenue());
        financials.setOperatingIncome(this.getOperatingIncome());
        financials.setNetIncome(this.getNetIncome());
        financials.setResearchAndDevelopment(this.getResearchAndDevelopment());
        financials.setOperatingExpense(this.getOperatingExpense());
        financials.setCurrentAssets(this.getCurrentAssets());
        financials.setTotalAssets(this.getTotalAssets());
        financials.setTotalLiabilities(this.getTotalLiabilities());
        financials.setCurrentCash(this.getCurrentCash());
        financials.setCurrentDebt(this.getCurrentDebt());
        financials.setTotalCash(this.getTotalCash());
        financials.setTotalDebt(this.getTotalDebt());
        financials.setShareholderEquity(this.getShareholderEquity());
        financials.setCashChange(this.getCashChange());
        financials.setCashFlow(this.getCashFlow());
        financials.setOperatingGainsLosses(this.getOperatingGainsLosses());

        financials.setStock(stockEntity);

        return financials;
    }
}