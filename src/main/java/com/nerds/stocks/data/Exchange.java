package com.nerds.stocks.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nerds.stocks.db.entities.ExchangeEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Exchange {
    String exchangeId;
    String exchangeName;
    String description;
    String city;
    String state;
    String country;
    int startTradingHour, startTradingMinute;
    int endTradingHour, endTradingMinute;
    int externdedTradingHour, extendedTradingMinute;
    TimeZone timezone = TimeZone.getTimeZone("EST");

    List<Holiday> holidays;

    public Exchange(ExchangeEntity entity) {
        this.exchangeId = entity.getExchangeId();
        this.exchangeName = entity.getExchangeName();
        this.description = entity.getDescription();
        this.city = entity.getCity();
        this.state = entity.getState();
        this.country = entity.getCountry();
        this.startTradingHour = entity.getStartTradingHour();
        this.startTradingMinute = entity.getStartTradingMinute();
        if(entity.getTimezone() == null) {
        	this.timezone = TimeZone.getTimeZone("EST");
        }else {
        	this.timezone = TimeZone.getTimeZone(entity.getTimezone());
        }
        
        this.holidays = entity.getHolidays().stream().filter(holidayEntity -> holidayEntity.getHoliday() != null)
                .map(holiday -> new Holiday(holiday)).collect(Collectors.toList());
    }
    
    public ExchangeEntity toExchangeEntity() {
    	ExchangeEntity exchangeEntity = new ExchangeEntity();
    	exchangeEntity.setExchangeId(this.getExchangeId());
    	exchangeEntity.setExchangeName(this.getExchangeName());
    	exchangeEntity.setDescription(this.getDescription());
    	exchangeEntity.setCity(this.getCity());
    	exchangeEntity.setState(this.getState());
    	exchangeEntity.setCountry(this.getCountry());
    	exchangeEntity.setStartTradingHour(this.getStartTradingHour());
    	exchangeEntity.setStartTradingMinute(this.getStartTradingMinute());
    	if(this.getTimezone() != null)
    		exchangeEntity.setTimezone(this.getTimezone().getID());
    	
    	if(this.getHolidays() != null)
    		exchangeEntity
    		.setHolidays(this
    				.getHolidays()
    				.stream()
    				.map(holiday -> holiday.toHolidayEntity(exchangeEntity))
    				.collect(Collectors.toList()));
    	
    	return exchangeEntity;
    }

    public Calendar getStartingTimeOfLastTradingDay() {
        Calendar currentTime = Calendar.getInstance(timezone);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        while (isHoliday(currentTime)) {
            logger.info(sdf.format(currentTime.getTime()) + " is a holiday, going to previous day");
            currentTime.add(Calendar.DATE, -1);
        }
        logger.info(sdf.format(currentTime.getTime()) + " is not a holiday, returning it");
        currentTime.set(Calendar.HOUR_OF_DAY, startTradingHour);
        currentTime.set(Calendar.MINUTE, startTradingMinute);
        currentTime.set(Calendar.SECOND, 0);
        currentTime.set(Calendar.MILLISECOND, 0);
        logger.info("Returned " + sdf.format(currentTime.getTime()));
        return currentTime;
    }

    public boolean isHoliday(Calendar givenCalendar) {
        Calendar todaysDate = Calendar.getInstance(timezone), calendar = Calendar.getInstance(timezone);
        calendar.setTimeInMillis(givenCalendar.getTimeInMillis()); // copy the calendar to the local time zone

        int minutesInStartTime = (startTradingHour * 60) + startTradingMinute;
        int currentMinutes = (calendar.get(Calendar.HOUR_OF_DAY) * 60) + calendar.get(Calendar.MINUTE);
        boolean isHoliday = false;
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == Calendar.SUNDAY || dayOfWeek == Calendar.SATURDAY) {
            isHoliday = true;
        } else if (todaysDate.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)
                && todaysDate.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)
                && todaysDate.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)
                && currentMinutes < minutesInStartTime) {
            isHoliday = true;
        } else {
            for (Holiday holiday : holidays) {
                Calendar holidayCalendar = holiday.getDate();
                if (holidayCalendar.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)
                        && holidayCalendar.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
                    isHoliday = true;
                    break;
                }
            }
        }
        return isHoliday;
    }

    static final Logger logger = LoggerFactory.getLogger(Exchange.class);
}
