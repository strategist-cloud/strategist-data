package com.nerds.stocks.data;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nerds.stocks.db.entities.EarningsEntity;
import com.nerds.stocks.db.entities.StockEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@Getter
@Setter
@NoArgsConstructor
public class Earnings {
    @JsonIgnore
    Fundamentals stock;

    private BigDecimal actualEPS;
    private BigDecimal consensusEPS;
    private BigDecimal estimatedEPS;
    private String announceTime;
    private BigDecimal numberOfEstimates;
    private BigDecimal EPSSurpriseDollar;
    private LocalDate EPSReportDate;
    private String fiscalPeriod;
    private LocalDate fiscalEndDate;
    private BigDecimal yearAgo;

    public Earnings(EarningsEntity statsObj, Fundamentals stock) {
        setActualEPS(statsObj.getActualEPS());
        setConsensusEPS(statsObj.getConsensusEPS());
        setEstimatedEPS(statsObj.getConsensusEPS());
        setAnnounceTime(statsObj.getAnnounceTime());
        setNumberOfEstimates(statsObj.getNumberOfEstimates());
        setEPSSurpriseDollar(statsObj.getEPSSurpriseDollar());
        setFiscalPeriod(statsObj.getFiscalPeriod());
        setYearAgo(statsObj.getYearAgo());

        if (statsObj.getFiscalEndDate() != null) {
            setFiscalEndDate(statsObj.getFiscalEndDate().toLocalDate());
        }

        if (statsObj.getEPSReportDate() != null) {
            setEPSReportDate(statsObj.getEPSReportDate().toLocalDate());
        }

        setStock(stock);
    }
    
    public EarningsEntity toEarningsEntity(StockEntity stock) {
    	EarningsEntity entity = new EarningsEntity();
    	
    	entity.setActualEPS(this.getActualEPS());
    	entity.setConsensusEPS(this.getConsensusEPS());
    	entity.setEstimatedEPS(this.getConsensusEPS());
    	entity.setAnnounceTime(this.getAnnounceTime());
    	entity.setNumberOfEstimates(this.getNumberOfEstimates());
    	entity.setEPSSurpriseDollar(this.getEPSSurpriseDollar());
    	entity.setFiscalPeriod(this.getFiscalPeriod());
    	entity.setYearAgo(this.getYearAgo());

        if(this.getFiscalEndDate() != null) {
        	entity.setFiscalEndDate(Date.valueOf(this.getFiscalEndDate()));
        }

        if(this.getEPSReportDate() != null) {
        	entity.setEPSReportDate(Date.valueOf(this.getEPSReportDate()));
        }

        entity.setStock(this.stock.toStockEntity());        
    	
    	return entity;
    }
}