package com.nerds.stocks.data;

import java.sql.Timestamp;

import com.nerds.stocks.db.entities.TransactionEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Transaction {
    public Transaction(TransactionEntity tranEntity) {
        this.price = tranEntity.getPrice();
        this.total = tranEntity.getTotal();
        this.orderTime = tranEntity.getOrderTime();
        this.count = tranEntity.getCount();
        if (tranEntity.getStock() != null) {
            this.exchange = tranEntity.getStock().getExchange();
            this.symbol = tranEntity.getStock().getSymbol();
            this.stockInfo = new Fundamentals(tranEntity.getStock());
        }
    }

    double price;
    double total;
    long count;
    Timestamp orderTime;
    String exchange;
    String symbol;
    Fundamentals stockInfo;

}