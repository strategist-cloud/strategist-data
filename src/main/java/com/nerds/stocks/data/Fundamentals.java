
package com.nerds.stocks.data;

import java.util.List;
import java.util.stream.Collectors;

import com.nerds.stocks.db.entities.StockEntity;

import org.springframework.util.CollectionUtils;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class Fundamentals {
    // Exchange exchange;
    String exchange;
    String symbol;
    String name;
    String sector;
    String industry;
    String description;
    String robinhoodId;
    String city;
    String state;
    Integer yearFounded;
    double marginInitialRatio;
    double maintenanceRatio;

    Stats stats;
    List<Financials> financials;
    List<Earnings> Earnings;

    Quote currentPrice;

    public Fundamentals(StockEntity stockEntity) {
        setExchange(stockEntity.getExchange());
        setSymbol(stockEntity.getSymbol());
        setIndustry(stockEntity.getIndustry());
        setSector(stockEntity.getSector());
        setRobinhoodId(stockEntity.getRobinhoodId());
        setYearFounded(stockEntity.getYearFounded());
        setCity(stockEntity.getCity());
        setState(stockEntity.getState());
        setDescription(stockEntity.getDescription());
        setName(stockEntity.getName());
        setRobinhoodId(stockEntity.getRobinhoodId());
        setStats(new Stats(stockEntity.getStats()));
        // List<RecentQuoteEntity> recentQuotes = stockEntity.getCurrentPrice();
        // if(!Collections.isEmpty(recentQuotes)) {
        // setCurrentPrice(Quote.getQuote(recentQuotes.get(0)));
        // }
        setCurrentPrice(Quote.getQuote(stockEntity.getCurrentPrice()));
        setFinancials(stockEntity.getFinancials().stream().map(entity -> new Financials(entity, this))
                .collect(Collectors.toList()));
    }

    public StockEntity toStockEntity() {
        StockEntity stock = new StockEntity();
        stock.setExchange(this.getExchange());
        stock.setIndustry(this.getIndustry());
        stock.setSector(this.getSector());
        stock.setSymbol(this.getSymbol());
        stock.setRobinhoodId(this.getRobinhoodId());
        stock.setYearFounded(this.getYearFounded());
        stock.setCity(this.getCity());
        stock.setState(this.getState());
        stock.setDescription(this.getDescription());
        stock.setName(this.getName());
        stock.setRobinhoodId(this.getRobinhoodId());
        if (this.getStats() != null) {
            stock.setStats(this.getStats().toStatsEntity(stock));
        }
        if (!CollectionUtils.isEmpty(financials)) {
            stock.setFinancials(this.getFinancials().parallelStream()
                    .map(financial -> financial.toFinancialsEntity(stock)).collect(Collectors.toList()));
        }

        return stock;
    }

}