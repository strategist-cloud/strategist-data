package com.nerds.stocks.data;

import java.util.Calendar;
import java.util.Date;

import com.nerds.stocks.db.entities.ExchangeEntity;
import com.nerds.stocks.db.entities.HolidayEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Holiday {
    String name;
    Calendar date;

    public Holiday(HolidayEntity entity) {
        this.name = entity.getName();
        if (entity.getHoliday() != null) {
        	this.date = Calendar.getInstance();
        	this.date.setTime(entity.getHoliday());
        }
    }
    
    public HolidayEntity toHolidayEntity(ExchangeEntity exchange) {
    	Date dateForCalendar = null;
    	if(date != null) {
    		dateForCalendar = date.getTime();
    	}
    	return new HolidayEntity(dateForCalendar, name, exchange);
    }
}