package com.nerds.stocks.data;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import com.nerds.stocks.db.entities.DailyQuoteEntity;
import com.nerds.stocks.db.entities.FiveMinQuoteEntity;
import com.nerds.stocks.db.entities.QuoteEntity;
import com.nerds.stocks.db.entities.RecentQuoteEntity;
import com.nerds.stocks.db.entities.StockEntity;
import com.nerds.stocks.db.entities.ThirtyMinQuoteEntity;

public class Quote {
    String stock;
    double closePrice;
    double openPrice;
    double highPrice;
    double lowPrice;
    long volume;
    Timestamp endTime;
    Timestamp startTime;

    public Quote() {

    }

    public static Quote getQuote(QuoteEntity quoteInDB) {

        if(quoteInDB == null) {
            return null;
        }

        Quote quote = new Quote();
        if(quoteInDB.getStock() != null) {
            quote.stock = quoteInDB.getStock().getSymbol();
        }
        quote.closePrice = quoteInDB.getClosePrice();
        quote.openPrice = quoteInDB.getOpenPrice();
        quote.highPrice = quoteInDB.getHighPrice();
        quote.lowPrice = quoteInDB.getLowPrice();
        quote.volume = quoteInDB.getVolume();
        quote.endTime = quoteInDB.getEndTime();
        quote.startTime = quoteInDB.getStartTime();

        return quote;
    }
    
    public QuoteEntity toQuoteEntity(StockEntity stock) {
    	QuoteEntity quoteEntity = new QuoteEntity();
    	loadQuoteEntity(quoteEntity, stock);
    	return quoteEntity;
    }
    
    public DailyQuoteEntity toDailyQuoteEntity(StockEntity stock) {
    	DailyQuoteEntity quoteEntity = new DailyQuoteEntity();
    	loadQuoteEntity(quoteEntity, stock);
    	return quoteEntity;
    }
    
    public FiveMinQuoteEntity toFiveMinQuoteEntity(StockEntity stock) {
    	FiveMinQuoteEntity quoteEntity = new FiveMinQuoteEntity();
    	loadQuoteEntity(quoteEntity, stock);
    	return quoteEntity;
    }
    
    public ThirtyMinQuoteEntity toThirtyMinQuoteEntity(StockEntity stock) {
    	ThirtyMinQuoteEntity quoteEntity = new ThirtyMinQuoteEntity();
    	loadQuoteEntity(quoteEntity, stock);
    	return quoteEntity;
    }
    
    public RecentQuoteEntity toRecentQuoteEntity(StockEntity stock) {
    	RecentQuoteEntity quoteEntity = new RecentQuoteEntity();
    	loadQuoteEntity(quoteEntity, stock);
    	return quoteEntity;
    }
    
    private void loadQuoteEntity(QuoteEntity quoteEntity, StockEntity stock) {
    	quoteEntity.setClosePrice(this.closePrice);
    	quoteEntity.setOpenPrice(this.openPrice);
    	quoteEntity.setHighPrice(this.highPrice);
    	quoteEntity.setLowPrice(this.lowPrice);
    	quoteEntity.setVolume(this.volume);
    	quoteEntity.setEndTime(this.endTime);
    	quoteEntity.setStartTime(this.startTime);
    	
    	quoteEntity.setStock(stock);
    }

    public static List<Quote> getQuoteList(List<QuoteEntity> quotesInDB) {
        if(quotesInDB == null) {
            return null;
        }
        return quotesInDB.stream().map(Quote::getQuote).collect(Collectors.toList());
    }

    public String getStock() {
        return this.stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public double getClosePrice() {
        return this.closePrice;
    }

    public void setClosePrice(double closePrice) {
        this.closePrice = closePrice;
    }

    public double getOpenPrice() {
        return this.openPrice;
    }

    public void setOpenPrice(double openPrice) {
        this.openPrice = openPrice;
    }

    public double getHighPrice() {
        return this.highPrice;
    }

    public void setHighPrice(double highPrice) {
        this.highPrice = highPrice;
    }

    public double getLowPrice() {
        return this.lowPrice;
    }

    public void setLowPrice(double lowPrice) {
        this.lowPrice = lowPrice;
    }

    public long getVolume() {
        return this.volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public Timestamp getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Timestamp getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public boolean equals(Quote second) {
        return (stock == second.getStock() || stock.equals(second.getStock()))
        && (startTime == second.startTime || (startTime != null && second.startTime != null && startTime.equals(second.startTime)))
        && (endTime == second.endTime || (endTime != null && second.endTime != null && endTime.equals(second.endTime)))
        && closePrice == second.closePrice;
    }

    public String toString() {
        return "" + stock + "," + startTime + "," + endTime + "," + closePrice;
    }
}