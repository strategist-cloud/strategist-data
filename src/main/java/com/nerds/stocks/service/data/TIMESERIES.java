package com.nerds.stocks.service.data;

public enum TIMESERIES {
	ONE_DAY, FIVE_DAY, ONE_MONTH, SIX_MONTH, ONE_YEAR, TWO_YEAR, FIVE_YEAR, TEN_YEAR
}