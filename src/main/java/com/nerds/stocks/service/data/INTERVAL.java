package com.nerds.stocks.service.data;

public enum INTERVAL {
    _5MIN, _30MIN, DAILY, WEEKLY
}