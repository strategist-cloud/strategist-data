package com.nerds.stocks.service.util;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nerds.stocks.data.Exchange;
import com.nerds.stocks.service.data.INTERVAL;
import com.nerds.stocks.service.data.TIMESERIES;

public class TimeUtil {

    public static Calendar getTimestamp(TIMESERIES timeSeries, Exchange exchange) {
        Calendar calendar = exchange.getStartingTimeOfLastTradingDay();
        switch (timeSeries) {
            case ONE_DAY:
                break;
            case FIVE_DAY:
                calendar.add(Calendar.DATE, -7);
                break;
            case ONE_MONTH:
                calendar.add(Calendar.MONTH, -1);
                break;
            case SIX_MONTH:
                calendar.add(Calendar.MONTH, -6);
                break;
            case ONE_YEAR:
                calendar.add(Calendar.YEAR, -1);
                break;
            case TWO_YEAR:
                calendar.add(Calendar.YEAR, -2);
                break;
            case FIVE_YEAR:
                calendar.add(Calendar.YEAR, -5);
                break;
            case TEN_YEAR:
                calendar.add(Calendar.YEAR, -10);
                break;
        }
        // calendar.set(Calendar.HOUR, 0);
        // calendar.set(Calendar.MINUTE, 0);
        // calendar.set(Calendar.SECOND, 0);
        // calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    public static INTERVAL getInterval(TIMESERIES timeSeries) {
        if(timeSeries == TIMESERIES.ONE_DAY
            || timeSeries == TIMESERIES.FIVE_DAY) {
            return INTERVAL._5MIN;
        }else if(timeSeries == TIMESERIES.ONE_MONTH) {
            return INTERVAL._30MIN;
        }else if(timeSeries == TIMESERIES.ONE_YEAR
            || timeSeries == TIMESERIES.SIX_MONTH){
            return INTERVAL.DAILY;
        }else {
            return INTERVAL.WEEKLY;
        }
    }

    public static TIMESERIES getTimeseries(INTERVAL interval) {
        if(interval == INTERVAL._5MIN) {
            return TIMESERIES.FIVE_DAY;
        }else if(interval == INTERVAL._30MIN) {
            return TIMESERIES.ONE_MONTH;
        }else if(interval == INTERVAL.DAILY) {
            return TIMESERIES.TEN_YEAR;
        }else {
            return TIMESERIES.TEN_YEAR;
        }
    }

    public static TIMESERIES getTimeseries(Calendar from) {
        TIMESERIES timeSeries = TIMESERIES.TEN_YEAR;

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0); calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0); calendar.set(Calendar.MILLISECOND, 0);

        calendar.add(Calendar.DATE, -7);
        long week = calendar.getTimeInMillis();
        calendar.add(Calendar.DATE, 7);
        calendar.add(Calendar.MONTH, -1);
        long month = calendar.getTimeInMillis();
        calendar.add(Calendar.MONTH, -11);
        long year = calendar.getTimeInMillis();
        long fromTime = from.getTimeInMillis();

        if(week <= fromTime) {
            timeSeries = TIMESERIES.FIVE_DAY;
        }else if(month <= fromTime) {
            timeSeries = TIMESERIES.ONE_MONTH;
        }else if(year <= fromTime) {
            timeSeries = TIMESERIES.ONE_YEAR;
        }else {
            timeSeries = TIMESERIES.TEN_YEAR;
        }

        return timeSeries;
    }

    static final Logger logger = LoggerFactory.getLogger(TimeUtil.class);

}